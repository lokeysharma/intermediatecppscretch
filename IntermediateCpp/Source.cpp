
#include <conio.h>
#include "Source.h"
#include <fstream>


namespace lokeys {


	void printWidth(const char* ch, int w) {
		
		int n = 0;
		for (; *ch != 0; ch++) {
			_putch(*ch);
			n++;
		}

		for (; n < w; n++)
		{
			_putch(' ');
		}
	}


	void print(const char* ch) {

		while (*ch != 0)
		{
			_putch(*ch);
			ch++;
		}
	}

	void read(char* buff, int maxSize) {

		const char* const pEnd = buff + maxSize;
		for (char c = _getch(); c != 13 && (buff + 1 < pEnd); c = _getch(), buff++)
		{
			_putch(c);
			*buff = c;
		}
		*buff = 0;
	}

	int str2int(const char* s)
	{
		// scaning for starting point.
		const char* p = s;

		for (; *p >= '0'&&*p <= '9'; p++);// run the loop untill it finds the end or invalid character.
		p--; // got the last valid chharacter.


		int val = 0;
		int place = 1;

		//convert the value if it is multi value;

		for (; p >= s; p--) {
			val += (*p - '0')*place;
			place *= 10;
		}
		return val;
	}


	void reverseStr(char* str) {

		char* strend = str;
		for (; *strend >= '0'&& *strend <= '9'; strend++);
		strend--;

		// swap with the front and end str values.
		for (; str <= strend; str++, strend--) {
			const char temp = *str;
			*str = *strend;
			*strend = temp;
		}
	}

	void strcpy(const char* pfirst, char* pSecond,int maxBuffSize) {
	
		int n = 0;
		for (; *pfirst != 0 && n+1<maxBuffSize; pfirst++, pSecond++,n++) {

			*pSecond = *pfirst;
		}
		*pSecond = 0;
	}


	void int2str(int val, char* buff, int size) {
			
		char* strStart = buff;

		for (; val > 0; val /= 10) {

			*buff = val % 10 + '0';
			buff++;
		}
		*buff = 0;
		reverseStr(strStart);
	}

	int fib(int n) {

		if (n < 2) {
			return n;
		}

		return (fib(n - 1) + fib(n - 2));
	}


	class Database {
	private:
		class Entry {
		private: 
			static constexpr int nameBufferSize = 10;
			char name[nameBufferSize];
			int value;
		public:
			Entry() = default;
			Entry(const char* name, int val)
				:
				value(val) {
				lokeys::strcpy(name, this->name,sizeof(this->name));
			}
			void Print()const {
				printWidth(name, nameBufferSize - 1);
				_putch('|');
				for (int i = 0; i < value; i++)
				{
					_putch('=');
				}
				_putch('\n');
			}

			void Serielize(std::ofstream& out) const
			{
				out.write(name, sizeof(name));
				out.write(reinterpret_cast<const char*> (&value), sizeof(value));
			}

			void Deserielize(std::ifstream& in)
			{
				in.read(name, sizeof(name));
				in.read(reinterpret_cast<char*> (&value), sizeof(value));
			}
		};

	private:
		static constexpr int maxNumberEntries = 16;
		Entry entries[maxNumberEntries];
		int currentNumberEntry = 0;
	public:
		void Add(const char* name, int val)
		{
			if(currentNumberEntry < maxNumberEntries )
				entries[currentNumberEntry++] = { name,val };
		}
		void Load(const char* fileName)
		{
			std::ifstream in(fileName, std::ios::binary);
			in.read(reinterpret_cast<char*>(&currentNumberEntry), sizeof(currentNumberEntry));
			for (int i = 0; i < currentNumberEntry; i++)
			{
				entries[i].Deserielize(in);
			}
		}
		void Save(const char* fileName) const
		{
			std::ofstream out(fileName, std::ios::binary);
			out.write(reinterpret_cast<const char*>(&currentNumberEntry), sizeof(currentNumberEntry));// will write the first 4 bytes of int and then we willlopp those
			
			for (int i = 0; i < currentNumberEntry; i++)
			{
				entries[i].Serielize(out);
			}
		}
		void Print() const
		{
			for (int i = 0; i < currentNumberEntry; i++)
			{
				entries[i].Print();
			}
		}
	};

	//maincode
	//print("Enter a number! \n");
	//char number[50];
	//read(number, 15);

	//int val = str2int(number);
	//// finding the fibonacci number.
	//int fibNumber = fib(val);

	//// checking str to int
	//char str[100];
	//int2str(fibNumber, str, 100);
	//print(str);
}


int main() {

	/*std::ifstream in("derp.txt");

	for (char c = in.get(); in.good();c=in.get()) {
		_putch(c);
	}

	if (in.bad()) {
		lokeys::print("Something happend!");
	}

	else if(in.eof)
	{
		lokeys::print("Successfully completed.");
	}*/

	/*
	const int value = 678478;
	std::ofstream out("output.dat", std::ios::binary);
	out.write(reinterpret_cast<const char*>(&value), sizeof(int));
	out.close();


	int readVal;
	std::ifstream in("output.dat", std::ios::binary);
	in.read(reinterpret_cast<char*>(&readVal), sizeof(int));
	in.close();

	char a[256];
	lokeys::int2str(readVal, a, 256);
	lokeys::print(a);
	*/
	

	// task to output the values like graph.
	lokeys::Database db;
	char buffer[256];
	char buffer2[256];
	bool isQuitted = false;

	do 
	{
		lokeys::print("Add, Load, Print, Save or Quit");
		char response = _getch();
		switch (response)
		{
		case 'l':
		{
			lokeys::print("\n Enter the file name");
			lokeys::read(buffer, sizeof(buffer));
			db.Load(buffer);
			_putch('\n');
			break;
		}

		case 's':
		{
			lokeys::print("\n Enter the file name");
			lokeys::read(buffer, sizeof(buffer));
			db.Save(buffer);
			_putch('\n');
			break;
		}

		case 'a':
		{
			lokeys::print("\n Enter  name");
			lokeys::read(buffer, sizeof(buffer));
			lokeys::print("\n Enter value");
			lokeys::read(buffer2, sizeof(buffer2));
			int val = lokeys::str2int(buffer2);
			db.Add(buffer, val);
			_putch('\n');
			break;
		}

		case 'p':
		{
			lokeys::print("\n\nVery well !");
			db.Print();
			break;
		}

		case 'q':
		{
			isQuitted = true;
			break;
		}

		default:
			break;
		}
	} while (!isQuitted);

	lokeys::print("Ends");
	_getch();
	return 0;
}